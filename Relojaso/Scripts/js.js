﻿//////Global values
//Constants
const updateLapse = 3000;//ms
const limitElements = 100;//max 100 elements
const timeContainer = document.getElementById("wm-component-time-container");
const countContainer = document.getElementById("wm-component-count-container");
const numberContainer = document.getElementById("wm-component-count-number");


//public functions
function ShowTime() {
    timeContainer.innerHTML = GetCurrentTime();
}
function GetCurrentTime(){
    let currentTime = new Date();
    let currentStringTime = IntegerToString(currentTime.getHours()) + ":" + IntegerToString(currentTime.getMinutes()) + ":" + IntegerToString(currentTime.getSeconds())
    return currentStringTime;
}
function IntegerToString(item) {
    let stringFormat = item.toString();
    if (item < 10) {
        stringFormat = "0" + stringFormat;
    }
    return stringFormat.toString();
}

function CountElements() {
    let currentValue = countContainer.value.toString();
    let currentNumberOfElements = limitElements - currentValue.length;

    if (currentNumberOfElements < 0) {
        countContainer.style.borderColor = "red";
    } else {
        countContainer.style.borderColor = "black";
    }
    numberContainer.innerHTML = "Restan: " + currentNumberOfElements.toString() + " letras.";
}


////Init
//Reloj
ShowTime();
setInterval(ShowTime, updateLapse);
//Contador
CountElements();
