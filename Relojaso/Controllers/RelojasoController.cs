﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Relojaso.Controllers
{
    public class RelojasoController : Controller
    {
        // GET: Relojaso
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReadFile()
        {
            int[] numbers = new int[4];



            return View();
        }

        private int[] OrderNumbers(int[] numbers)
        {
            int tmpNumber = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                for (int j = 0; j < numbers.Length - 1; j++)
                {
                    if (numbers[j] > numbers[j + 1])
                    {
                        tmpNumber = numbers[j + 1];
                        numbers[j + 1] = numbers[j];
                        numbers[j] = tmpNumber;
                    }
                }
            }
            return numbers;
        }
    }
}